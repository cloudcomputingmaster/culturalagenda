#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
from datetime import datetime, timedelta
from urllib import urlencode, urlopen
from xml.dom import minidom

import jinja2
import webapp2
from google.appengine.api import images,users
from google.appengine.api.images import NotImageError
from google.appengine.ext import db

from flickr_keys import FLICKR_KEY
from models import Events

TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), 'templates')
jinja_environment = \
    jinja2.Environment(loader=jinja2.FileSystemLoader(TEMPLATE_DIR))

import sys

reload(sys)
sys.setdefaultencoding('utf-8')

class BaseHandler(webapp2.RequestHandler):
    def render_template(
            self,
            filename,
            template_values,
            **template_args
    ):
        template = jinja_environment.get_template(filename)
        self.response.out.write(template.render(template_values))


def get_user():
    user = users.get_current_user()
    if user:
        nickname = user.nickname()
        logout_url = users.create_logout_url('/')
        greeting = 'Bienvenido, {}! <a href="{}" class="btn btn-default">Cerrar sesión</a>'.format(nickname, logout_url)
    else:
        login_url = users.create_login_url('/')
        greeting = '<a href="{}" class="btn btn-info">Iniciar sesión</a>'.format(login_url)
    # user_text = '<html><body>{}</body></html>'.format(greeting)
    user_text = greeting
    return user, user_text


class ShowAdds(BaseHandler):
    def get(self):
        # date = datetime.today().strftime("%Y/%m/%d")
        # dateType = datetime.strptime(date,"%Y/%m/%d").date()
        # print(dateType)
        adds = Events.all()
        # date = (datetime.today()).strftime("%HH:%MM")
        # timeType = datetime.strptime(date,"%HH:%MM").time()
        results=[x for x in adds if x.datetime >= datetime.now()]

        user, user_text = get_user()
        if user:
            user = user.nickname()
        # self.render_template('adds.html', {'adds': results})
        self.render_template('adds.html', {'adds': results, 'user': user, 'user_text': user_text})


class NewAdd(BaseHandler):

    def post(self):
        user = users.get_current_user()
        if user:
            add = Events(
                name=str(self.request.get("inputName")),
                category=str(self.request.get("inputCategory")).encode('utf-8').strip(),
                location=str(self.request.get("inputLocation")).encode('utf-8').strip(),
                description=str(self.request.get("inputDescription")),
                datetime=datetime.combine(datetime.strptime(self.request.get("inputDate"),"%d/%m/%Y").date(),datetime.strptime(self.request.get("inputHour"), "%H:%M").time()),
                price=str(format(float(self.request.get("inputPrice")), '.2f')),
                author=users.get_current_user().nickname(),
            )

            tag = self.request.get("inputFlickr")
            if tag:
                add.tag = tag
            else:
                add.tag = None

            poster = self.request.get("inputImage")
            if poster:
                poster = images.resize(poster, 320, 256)
                add.poster = poster

            add.put()
            return webapp2.redirect('/')
        else:
            raise Exception('No puedes añadir un evento, debes iniciar sesión primero!!')
    def get(self):
        self.render_template('new.html', {})


class EditAdd(BaseHandler):

    def post(self, add_id):
        user = users.get_current_user()
        iden = int(add_id)
        add = db.get(db.Key.from_path('Events', iden))
        if add.author == user.nickname():
            add.name = str(self.request.get('inputName'))
            add.category = str(self.request.get('inputCategory')).encode('utf-8').strip()
            add.location = str(self.request.get("inputLocation")).encode('utf-8').strip()
            add.description = str(self.request.get("inputDescription"))
            add.datetime = datetime.combine(datetime.strptime(self.request.get("inputDate"),"%d/%m/%Y").date(),datetime.strptime(self.request.get("inputHour"), "%H:%M").time())
            add.price = str(format(float(self.request.get("inputPrice")), '.2f'))
            poster = self.request.get("inputImage")
            tag = str(self.request.get("inputFlickr"))

            try:
                poster = images.resize(poster, 320, 256)
            except NotImageError:
                poster = add.poster

            if tag:
                add.tag = tag
            elif tag=="Ninguno":
                add.tag = None
            else:
                add.tag = None

            add.poster = poster
            add.put()
            return webapp2.redirect('/edit/'+add_id)
        else:
            raise Exception('No puedes editar este evento!!')

    def get(self, add_id):
        user, user_text = get_user()
        self.response.write(user_text)
        iden = int(add_id)
        add = db.get(db.Key.from_path('Events', iden))
        if add.author == user.nickname():
            poster = "-1"
            if add.poster:
                poster = json.dumps(add.poster.encode("base64"))

            flickr=[]
            if add.tag:
                data = _doget('flickr.photos.search', auth=False, text=add.tag, per_page='6')
                if data:
                    photos = data.getElementsByTagName("photo")
                    for photo in photos:
                        farm_id = 1
                        server_id = photo.attributes['server'].value
                        photo_id = photo.attributes['id'].value
                        secret = photo.attributes['secret'].value
                        photo_url = 'http://farm{farm_id}.staticflickr.com/{server_id}/{photo_id}_{secret}.jpg'.format(
                            farm_id=farm_id, server_id=server_id, photo_id=photo_id, secret=secret)
                        # self.response.write('<img src="{0}">'.format(photo_url))
                        flickr.append(photo_url)
                        # print(photo_url)
            else:
                add.tag="Ninguno"

            photos = "-1"
            if flickr:
                photos = json.dumps(flickr)

            self.render_template('edit.html', {'add': add, 'poster': poster,"photos":photos,"numphotos":len(flickr)})
        else:
            raise Exception('No puedes editar este evento!!')

class DeleteAdd(BaseHandler):
    def get(self, add_id):
        user, user_text = get_user()
        self.response.write(user_text)

        iden = int(add_id)
        add = db.get(db.Key.from_path('Events', iden))
        if add.author == user.nickname():
            db.delete(add)
            return webapp2.redirect('/')
        else:
            raise Exception('No puedes borrar este evento!!')


class SeeAdd(BaseHandler):
    posterOld = ""

    def get(self, add_id):
        global posterOld
        iden = int(add_id)
        add = db.get(db.Key.from_path('Events', iden))
        poster = "-1"
        tag = "-1"
        if add.poster:
            poster = json.dumps(add.poster.encode("base64"))

        flickr=[]
        if add.tag:
            data = _doget('flickr.photos.search', auth=False, text=add.tag, per_page='6')
            if data:
                photos = data.getElementsByTagName("photo")
                for photo in photos:
                    farm_id = 1
                    server_id = photo.attributes['server'].value
                    photo_id = photo.attributes['id'].value
                    secret = photo.attributes['secret'].value
                    photo_url = 'http://farm{farm_id}.staticflickr.com/{server_id}/{photo_id}_{secret}.jpg'.format(
                        farm_id=farm_id, server_id=server_id, photo_id=photo_id, secret=secret)
                    # self.response.write('<img src="{0}">'.format(photo_url))
                    flickr.append(photo_url)
                    # print(photo_url)
        else:
            add.tag="Ninguno"

        photos = "-1"
        if flickr:
            photos = json.dumps(flickr)

        self.render_template('see.html', {'add': add, 'poster': poster,"photos":photos,"numphotos":len(flickr)})

HOST = 'https://api.flickr.com'
API = '/services/rest'

def _doget(method, auth=False, **params):
    #print "***** do get %s" % method

    params = params
    url = '%s%s/?api_key=%s&method=%s&%s'%\
      (HOST, API, FLICKR_KEY, method, urlencode(params))

    res = urlopen(url)
    res = res.read()

    return minidom.parseString(res)