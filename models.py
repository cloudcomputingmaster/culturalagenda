from google.appengine.ext import db


class Events(db.Model):

    author = db.EmailProperty()
    name = db.StringProperty(required=True)
    category = db.StringProperty(required=True)
    location = db.StringProperty(required=True)
    description = db.StringProperty(required=True)
    datetime = db.DateTimeProperty(required=False)
    price = db.StringProperty(required=True)
    poster = db.BlobProperty(required=False)
    tag = db.StringProperty(required=False)
