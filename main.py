from views import ShowAdds, NewAdd, DeleteAdd, EditAdd,SeeAdd
import webapp2

app = webapp2.WSGIApplication([
        ('/', ShowAdds), 
        ('/new', NewAdd), 
        ('/edit/([\d]+)', EditAdd),
        ('/delete/([\d]+)', DeleteAdd),
        ('/see/([\d]+)', SeeAdd),
        ],
        debug=True)
